// import 'package:einfach_feiern/configs/constanst/app_colors.dart';
// import 'package:einfach_feiern/presentation/base/base.dart';
// import 'package:einfach_feiern/resource/model/model.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class WeddingCheckListModel extends BaseViewModel {
//   TextEditingController nameController = TextEditingController();
//   TextEditingController categoryController = TextEditingController();
//   TextEditingController newTaskController = TextEditingController();
//
//   FocusNode inputNode = FocusNode();
//
//   bool _doubleValue = true;
//   bool _valueSucces = true;
//
//   bool get doubleValue => _doubleValue;
//   set doubleValue(bool doubleValue) {
//     _doubleValue = doubleValue;
//     notifyListeners();
//   }
//
//   bool get valueSucces => _valueSucces;
//   set valueSucces(bool valueSucces) {
//     _valueSucces = valueSucces;
//     notifyListeners();
//   }
//
//   List<NameModel> names = [
//     NameModel(
//       idName: 1,
//       idTitle: 1,
//       name: 'Task1',
//       isCheck: false,
//       isEditext: false,
//     ),
//     NameModel(
//         idName: 2, idTitle: 1, name: 'Task2', isCheck: false, isEditext: false),
//     NameModel(
//         idName: 3, idTitle: 1, name: 'Task3', isCheck: false, isEditext: false),
//     NameModel(
//         idName: 4, idTitle: 2, name: 'Task1', isCheck: false, isEditext: false),
//     NameModel(
//         idName: 5, idTitle: 2, name: 'Task2', isCheck: false, isEditext: false),
//     NameModel(
//         idName: 6, idTitle: 3, name: 'Task3', isCheck: false, isEditext: false),
//     NameModel(
//         idName: 7, idTitle: 3, name: 'Task1', isCheck: false, isEditext: false),
//   ];
//   List<NameModel> namesPopUp = [];
//   int get namesPopUpLeng => namesPopUp.length;
//   set namesPopUpLeng(int a) {
//     namesPopUp.length = a;
//     notifyListeners();
//   }
//
//   List<Titile> title = [
//     Titile(idTitle: 1, nameTitle: 'Agape'),
//     Titile(idTitle: 2, nameTitle: 'Anreise'),
//     Titile(idTitle: 3, nameTitle: 'Bar')
//   ];
//
//   List<CategoryContent> get categoryContents => title
//       .map((title) => CategoryContent(
//           titile: title,
//           namesmodel:
//               names.where((names) => names.idTitle == title.idTitle).toList()))
//       .toList();
//
//   init() async {}
//
//   findMaxidNameModel() {
//     NameModel max = names.reduce((a, b) => a.idName > b.idName ? a : b);
//     return max.idTitle + 1;
//   }
//
//   addItemByCategory(int idTitle) {
//     NameModel max = names.reduce((a, b) => a.idName > b.idName ? a : b);
//
//     names.insert(
//         0,
//         NameModel(
//             idTitle: idTitle,
//             name: newTaskController.text,
//             isCheck: false,
//             isEditext: false,
//             idName: findMaxidNameModel()));
//     newTaskController.clear();
//     Navigator.of(context).pop();
//     notifyListeners();
//   }
//
//   addItem() {
//     var newList = title.firstWhere(
//         (element) =>
//             element.nameTitle.toLowerCase() ==
//             categoryController.text.toLowerCase(),
//         orElse: () => null);
//     if (categoryController.text.length > 0) {
//       if (newList != null) {
//         insertItemAlreadyExist(newList);
//         Navigator.of(context).pop();
//         notifyListeners();
//       } else {
//         insertItemNotExist();
//         Navigator.of(context).pop();
//         notifyListeners();
//       }
//     } else {}
//   }
//
//   insertItemAlreadyExist(var newList) {
//     names.insert(
//         0,
//         NameModel(
//             idTitle: newList.idTitle,
//             name: newTaskController.text,
//             isCheck: false,
//             isEditext: false,
//             idName: findMaxidNameModel()));
//     newTaskController.clear();
//   }
//
//   insertItemNotExist() {
//     Titile max = title.reduce((a, b) => a.idTitle > b.idTitle ? a : b);
//     title.insert(0,
//         Titile(idTitle: max.idTitle + 1, nameTitle: categoryController.text));
//     names.insert(
//         0,
//         NameModel(
//             idTitle: max.idTitle + 1,
//             name: newTaskController.text,
//             isCheck: false,
//             isEditext: false,
//             idName: findMaxidNameModel()));
//     newTaskController.clear();
//   }
//
//   checkIsCheck(bool val, NameModel name) {
//     name.isCheck = val;
//     notifyListeners();
//   }
//
//   checkIsEditText(bool val, NameModel name) {
//     name.isEditext = val;
//     nameController.text = name.name;
//     notifyListeners();
//   }
//
//   removeItemById(NameModel name) {
//     names.removeWhere((item) => item.idName == name.idName);
//     notifyListeners();
//   }
//
//   removeListItemIdCategory(int idTitle) {
//     title.removeWhere((item) => item.idTitle == idTitle);
//     names.removeWhere((item) => item.idTitle == idTitle);
//     notifyListeners();
//   }
//
//   showAlertDialog(BuildContext context, NameModel name, Size _size) {
//     Widget cancelButton = TextButton(
//       child: Text(
//         "JA",
//         style: TextStyle(color: AppColors.title, fontWeight: FontWeight.bold),
//       ),
//       onPressed: () {
//         removeItemById(name);
//         Navigator.of(context).pop(); // dismiss dialog
//       },
//     );
//     Widget continueButton = TextButton(
//       child: Text(
//         "NEIN",
//         style: TextStyle(color: AppColors.title, fontWeight: FontWeight.bold),
//       ),
//       onPressed: () {
//         Navigator.of(context).pop();
//       },
//     );
//
//     // set up the AlertDialog
//     AlertDialog alert = AlertDialog(
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.all(Radius.circular(15))),
//       content: Text("Diese Aufgabe wirklich löschen??"),
//       actions: [
//         Container(
//           child: Row(
//             children: [
//               Expanded(
//                 flex: 1,
//                 child: Container(color: AppColors.grey, child: cancelButton),
//               ),
//               SizedBox(
//                 width: 3,
//               ),
//               Expanded(
//                 flex: 1,
//                 child:
//                     Container(color: AppColors.grey300, child: continueButton),
//               ),
//             ],
//           ),
//         )
//       ],
//     );
//
//     // show the dialog
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return alert;
//       },
//     );
//   }
//
//   showAlertDialogTitle(BuildContext context, int idTitle, Size _size) {
//     Widget cancelButton = TextButton(
//       child: Text(
//         "JA",
//         style: TextStyle(color: AppColors.title, fontWeight: FontWeight.bold),
//       ),
//       onPressed: () {
//         removeListItemIdCategory(idTitle);
//         Navigator.of(context).pop(); // dismiss dialog
//       },
//     );
//     Widget continueButton = TextButton(
//       child: Text(
//         "NEIN",
//         style: TextStyle(color: AppColors.title, fontWeight: FontWeight.bold),
//       ),
//       onPressed: () {
//         Navigator.of(context).pop();
//       },
//     );
//
//     // set up the AlertDialog
//     AlertDialog alert = AlertDialog(
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.all(Radius.circular(15))),
//       content: Text("Diese Aufgabe wirklich löschen??"),
//       actions: [
//         Container(
//           child: Row(
//             children: [
//               Expanded(
//                 flex: 1,
//                 child: Container(color: AppColors.grey, child: cancelButton),
//               ),
//               SizedBox(
//                 width: 3,
//               ),
//               Expanded(
//                 flex: 1,
//                 child:
//                     Container(color: AppColors.grey300, child: continueButton),
//               ),
//             ],
//           ),
//         )
//       ],
//     );
//
//     // show the dialog
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return alert;
//       },
//     );
//   }
// }
