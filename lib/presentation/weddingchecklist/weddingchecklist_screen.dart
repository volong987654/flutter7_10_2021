// import 'package:einfach_feiern/configs/configs.dart';
// import 'package:einfach_feiern/presentation/base/base.dart';
// import 'package:einfach_feiern/presentation/weddingchecklist/weddingchecklist_viewmodel.dart';
// import 'package:einfach_feiern/presentation/widgets/widget.dart';
// import 'package:einfach_feiern/resource/model/name.dart';
// import 'package:flutter/material.dart';
//
// class WeddingCheckListScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BaseWidget<WeddingCheckListModel>(
//         viewModel: WeddingCheckListModel(),
//         childDesktop: _builDeskTop(context),
//         childTablet: _builTablet(context),
//         onViewModelReady: (viewModel) {
//           viewModel..init();
//         },
//         child: _buildBody(context),
//         childMobile: _buildBodyMobile(context),
//         builder: (context, viewModel, child) {
//           return Scaffold(
//             body: _buildbody(context, viewModel),
//           );
//         });
//   }
//
//   Widget _buildBodyMobile(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Center(
//         child: Text('app_name'),
//       ),
//     );
//   }
//
//   Widget _buildBody(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Center(
//         child: Text('app_name'),
//       ),
//     );
//   }
//
//   Widget _builDeskTop(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Center(
//         child: Text('app_name'),
//       ),
//     );
//   }
//
//   Widget _builTablet(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Center(
//         child: Text('app_name'),
//       ),
//     );
//   }
//
//   Widget _buildbody(BuildContext context, WeddingCheckListModel _viewmodel) {
//     Size _size = MediaQuery.of(context).size;
//     return Container(
//       color: Colors.white,
//       width: _size.width,
//       height: _size.height,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           SizedBox(
//             height: _size.height * 0.03,
//           ),
//           _textCheckliste(context, _size),
//           Expanded(flex: 4, child: _list(context, _viewmodel, _size)),
//           Expanded(
//               flex: 1,
//               child: Column(
//                 children: [
//                   WidgetAddItem(
//                     onTap: () {
//                       _diLog(context, true, _size, _viewmodel);
//                     },
//                     size: _size,
//                     fontSizeText: 30,
//                   ),
//                   WidgetSaveList(onTap: () {}, name: AppValues.appCheckliste)
//                 ],
//               )),
//         ],
//       ),
//     );
//   }
//
//   Widget _textCheckliste(context, _size) {
//     return WidgetTextCheckList(
//         nameCheckList: 'Checkliste Hochzeit', size: _size);
//   }
//
//   Widget _list(context, _viewmodel, _size) {
//     return Padding(
//       padding: const EdgeInsets.all(12.0),
//       child: Container(
//           // height: _size.height * 0.02,
//           decoration: BoxDecoration(
//             border: Border.all(color: AppColors.title),
//           ),
//           child: ListView.builder(
//             shrinkWrap: true,
//             itemCount: _viewmodel.categoryContents.length,
//             itemBuilder: (context, index) {
//               return Column(
//                 children: [
//                   _buildTitle(context, _viewmodel, _size, index),
//                   ..._viewmodel.categoryContents[index].namesmodel
//                       .map((NameModel name) {
//                     return _buildRow(context, _viewmodel, _size, index, name);
//                   }),
//                 ],
//               );
//             },
//           )),
//     );
//   }
//
//   Widget _buildTitle(
//     BuildContext context,
//     WeddingCheckListModel _viewmodel,
//     Size _size,
//     int index,
//   ) {
//     return WidgetTitle(
//       size: _size,
//       nameTitle: _viewmodel.categoryContents[index].titile.nameTitle,
//       widgetDelete: WidgetDeleteItem(
//           onTap: () {
//             _viewmodel.showAlertDialogTitle(context,
//                 _viewmodel.categoryContents[index].titile.idTitle, _size);
//           },
//           name: 'X'),
//       widgetAddItem: WidgetAddItem(
//         onTap: () {
//           _diLogAddItembyCatygory(context, true, _size, _viewmodel, index);
//         },
//         size: _size,
//         fontSizeText: 30,
//       ),
//     );
//   }
//
//   Widget _buildRow(context, _viewmodel, _size, index, name) {
//     return WidgetRowItem(
//       size: _size,
//       buildLabeledCheckbox:
//           _buildLabeledCheckbox(context, _viewmodel, _size, index, name),
//       buildName: _buildName(
//         context,
//         _viewmodel,
//         _size,
//         index,
//         name,
//       ),
//       buildDeleteItem:
//           _buildRemoveItem(context, _viewmodel, _size, index, name),
//       buildMoveItem: _buildMoveItem(context, _viewmodel, _size, index, name),
//     );
//   }
//
//   Widget _buildLabeledCheckbox(context, _viewmodel, _size, index, name) {
//     return WidgetLabeledCheckbox(
//         size: _size,
//         padding: EdgeInsets.symmetric(horizontal: 20.0),
//         value: name.isCheck,
//         onChanged: (val) {
//           _viewmodel.checkIsCheck(val, name);
//           // name.isCheck = val;
//         });
//   }
//
//   Widget _buildRemoveItem(context, _viewmodel, _size, index, name) {
//     return WidgetDeleteItem(
//         onTap: () {
//           _viewmodel.showAlertDialog(context, name, _size);
//         },
//         name: 'X');
//   }
//
//   Widget _buildMoveItem(context, _viewmodel, _size, index, name) {
//     return WidgetMoveItem(nameRow: AppImages.arrows, nameRows: AppImages.arrow);
//   }
//
//   Widget _buildName(context, _viewmodel, _size, index, name) {
//     return WidgetName(
//       value: name.isCheck,
//       size: _size,
//       name: name.name,
//       controller: _viewmodel.nameController,
//       onTap: () {
//         _viewmodel.doubleValue = !_viewmodel.doubleValue;
//       },
//       doubleValue: name.isEditext,
//       onTapAccept: () {
//         // _viewmodel.checkIsEditText(value, name);
//       },
//       onChanged: (bool value) {
//         _viewmodel.checkIsEditText(value, name);
//       },
//       valueSucces: _viewmodel.valueSucces,
//       onTapValueSucces: () {
//         _viewmodel.valueSucces = !_viewmodel.valueSucces;
//       },
//     );
//   }
//
//   _diLog(context, _fromTop, size, viewmodel) {
//     showGeneralDialog(
//       barrierLabel: "Label",
//       barrierDismissible: true,
//       barrierColor: Colors.black.withOpacity(0.5),
//       transitionDuration: Duration(milliseconds: 700),
//       context: context,
//       pageBuilder: (BuildContext context, anim1, anim2) {
//         return Align(
//           alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
//           child: Container(
//             height: size.height * 0.5,
//             child: _diaLog(context, size, viewmodel),
//             margin: EdgeInsets.only(
//                 top: size.height * 0.2, left: 12, right: 12, bottom: 0),
//             decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(40),
//             ),
//           ),
//         );
//       },
//       transitionBuilder: (context, anim1, anim2, child) {
//         return SlideTransition(
//           position:
//               Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0))
//                   .animate(anim1),
//           child: child,
//         );
//       },
//     );
//   }
//
//   Widget _diaLog(context, Size size, viewmodel) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Center(
//         child: Column(
//           children: [
//             SizedBox(
//               height: size.height * 0.1,
//             ),
//             _buildCategoryNew(context, size, viewmodel),
//             SizedBox(
//               height: size.height * 0.01,
//             ),
//             _buildNewTask(context, size, viewmodel),
//             SizedBox(
//               height: size.height * 0.03,
//             ),
//             _buildAddItemDilog(context, size, viewmodel),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildCategoryNew(
//       BuildContext context, Size size, WeddingCheckListModel viewmodel) {
//     return TextField(
//       controller: viewmodel.categoryController,
//       decoration: InputDecoration(
//         border: OutlineInputBorder(),
//         labelText: 'Category',
//       ),
//     );
//   }
//
//   Widget _buildNewTask(
//       BuildContext context, Size size, WeddingCheckListModel viewmodel) {
//     return TextField(
//       controller: viewmodel.newTaskController,
//       decoration: InputDecoration(
//         border: OutlineInputBorder(),
//         labelText: 'New Task',
//       ),
//     );
//   }
//
//   Widget _buildAddItemDilog(
//       BuildContext context, Size size, WeddingCheckListModel viewmodel) {
//     return WidgetSaveList(
//         onTap: () {
//           viewmodel.addItem();
//         },
//         name: 'Add');
//   }
//
//   _diLogAddItembyCatygory(context, _fromTop, size, viewmodel, int index) {
//     showGeneralDialog(
//       barrierLabel: "Label",
//       barrierDismissible: true,
//       barrierColor: Colors.black.withOpacity(0.5),
//       transitionDuration: Duration(milliseconds: 700),
//       context: context,
//       pageBuilder: (BuildContext context, anim1, anim2) {
//         return Align(
//           alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
//           child: Container(
//             height: size.height * 0.5,
//             child: _diaLogAddItembyCatygory(context, size, viewmodel, index),
//             margin: EdgeInsets.only(
//                 top: size.height * 0.2, left: 12, right: 12, bottom: 0),
//             decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(40),
//             ),
//           ),
//         );
//       },
//       transitionBuilder: (context, anim1, anim2, child) {
//         return SlideTransition(
//           position:
//               Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0))
//                   .animate(anim1),
//           child: child,
//         );
//       },
//     );
//   }
//
//   Widget _diaLogAddItembyCatygory(context, Size size, viewmodel, int index) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Center(
//         child: Column(
//           children: [
//             SizedBox(
//               height: size.height * 0.1,
//             ),
//             _buildNewTaskByCategry(context, size, viewmodel),
//             SizedBox(
//               height: size.height * 0.01,
//             ),
//             _buildAddItemByCategory(context, size, viewmodel, index),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildNewTaskByCategry(
//       BuildContext context, Size size, WeddingCheckListModel viewmodel) {
//     return TextField(
//       controller: viewmodel.newTaskController,
//       decoration: InputDecoration(
//         border: OutlineInputBorder(),
//         labelText: 'New Task',
//       ),
//     );
//   }
//
//   Widget _buildAddItemByCategory(BuildContext context, Size size,
//       WeddingCheckListModel viewmodel, int index) {
//     return WidgetSaveList(
//         onTap: () {
//           viewmodel.addItemByCategory(
//               viewmodel.categoryContents[index].titile.idTitle);
//         },
//         name: 'Add');
//   }
// }
