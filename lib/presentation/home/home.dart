import 'package:duongguixd/configs/constanst/app_colors.dart';
import 'package:duongguixd/configs/constanst/app_styles.dart';
import 'package:duongguixd/presentation/widgets/background.dart';
import 'package:duongguixd/presentation/widgets/loginpage.dart';
import 'package:duongguixd/presentation/widgets/resigner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../routers.dart';
class Introduce extends StatelessWidget {
  Introduce({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  Image.asset(
                    "assets/image/signup_top.png",
                    color: Colors.black,
                    width: 20,
                  ),
                  Text('Kleinc',
                    style: AppStyles.titileStyle.copyWith(decoration: TextDecoration.none),

                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              Text('The Right Address \nFor Shopping \nAnyday',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                  color: Colors.black,
                  fontFamily: "Poppins",
                    decoration: TextDecoration.none
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text('It is now very easy to reach \nthe best quality among all ',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black.withOpacity(0.7),
                    decoration: TextDecoration.none
                ),
              ),
              SizedBox(
                height: size.width *0.4,
              ),
              Resignter(
                text: "Resignter",
                onTap: () {
                  Navigator.of(context).pushNamed(AppRoutes.authRegister);
                },
                color: AppColors.button,
              ),
              SizedBox(
                height: size.width *0.05,
              ),
              ButtonLogin(
                text: "Login",
                onTap: () {
                  Navigator.of(context).pushNamed(AppRoutes.authLogin);
                },
                color: AppColors.buttonLogin,
              ),
            ],
          ),
        ),
    );
  }
}
