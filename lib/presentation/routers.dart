import 'package:duongguixd/presentation/signin/signin_screen.dart';
import 'package:duongguixd/presentation/signup/signup_screen.dart';
import 'package:flutter/material.dart';

import 'home/home.dart';
import 'list_home/list_home.dart';



class AppRoutes {
  AppRoutes._();

  static const String authLogin = '/auth-login';
  static const String authRegister = '/auth-register';
  static const String home = '/home';

  static Map<String, WidgetBuilder> define() {
    return {
      authLogin: (context) => Login(),
      authRegister: (context) => RegisterPage(),

      home: (context) => Introduce(),
    };
  }
}