import 'package:duongguixd/configs/constanst/app_colors.dart';
import 'package:duongguixd/configs/constanst/app_styles.dart';
import 'package:duongguixd/presentation/widgets/background.dart';
import 'package:duongguixd/presentation/widgets/iconbutton.dart';
import 'package:duongguixd/presentation/widgets/resigner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../routers.dart';
class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  Widget _showUserNameInput(){
    return  Padding(
      padding: EdgeInsets.only(top: 15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'First Name',
        ),
      ),
    );
  }
  Widget _showUserLastNameInput(){
    return  Padding(
      padding: EdgeInsets.only(top: 15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Last Name',
        ),
      ),
    );
  }
  Widget _showEmailInput(){
    return  Padding(
      padding: EdgeInsets.only(top: 15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Volong987654@gmail.com',
        ),
      ),
    );
  }
  Widget _showPassWordInput(){
    return  Padding(
      padding: EdgeInsets.only(top: 15.0,bottom: 15),
      child: TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(),
          hintText: 'Phone or Mail',
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Background(
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              // key: _formKey,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:[
                        Image.asset(
                          "assets/image/signup_top.png",
                          color: Colors.black,
                          width: size.width * 0.03,
                        ),
                        Text('Kleinc',
                          style: AppStyles.titileStyle,
                        ),
                      ],
                    ),
                    Text('Let s Register',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Colors.black,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('It is now very easy to reach ? Login ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black.withOpacity(0.7),
                      ),
                    ),
                    _showUserNameInput(),
                    _showUserLastNameInput(),
                    _showEmailInput(),
                    _showPassWordInput(),
                    Resignter(
                      text: "Resignter",
                      onTap: () {
                        Navigator.of(context).pushNamed(AppRoutes.authRegister);
                      },
                      color: AppColors.button,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                   Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          iconbutton_Footer(
                            imgSrc:"assets/image/facbook.svg",
                            text: "Fcabook",
                          ),
                          iconbutton_Footer(
                            imgSrc:"assets/image/facbook.svg",
                            text: "Gmail",
                          ),
                        ],
                      ),

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
