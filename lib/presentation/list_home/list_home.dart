import 'package:duongguixd/configs/constanst/app_colors.dart';
import 'package:duongguixd/presentation/widgets/background_home.dart';
import 'package:duongguixd/presentation/widgets/button_next.dart';
import 'package:flutter/material.dart';

import '../routers.dart';
class List_Home extends StatelessWidget {
  const List_Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Background_Home(
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('The Right Address \nFor Shopping \nAnyday',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 32,
              color: Colors.black,
              fontFamily: "Poppins",
                decoration: TextDecoration.none
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text('It is now very easy to reach \nthe best quality among all ',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.black.withOpacity(0.7),
                decoration: TextDecoration.none
            ),
          ),
          SizedBox(
            height: 120,
          ),
          ButtonNext(
            text: "Next",
            onTap: () {
              Navigator.of(context).pushNamed(AppRoutes.home);
            },
            color: AppColors.button,
          ),
        ],
      ),
    );
  }
}
