import 'package:flutter/material.dart';

class Resignter extends StatelessWidget {
  String  text;
  final Size size;
  Function onTap;
  final  color, textColor;
  Resignter({
    @required this.text,
    @required this.onTap,
    @required this.size,
    this.color ,
    this.textColor = Colors.white,
  }) ;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
        alignment: Alignment.center,
        // width: size.width*0.68,
      width: size.width*0.9,
        height: size.height*0.06,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: GestureDetector(
          onTap: () {
            onTap();
          },
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.bold,
                decoration: TextDecoration.none
            ),
          ),
        ),

    );
  }
}
