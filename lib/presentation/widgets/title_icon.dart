import 'package:duongguixd/configs/constanst/app_styles.dart';
import 'package:flutter/material.dart';

class Title_Icon extends StatelessWidget {
  String  text;
  Title_Icon ({
    @required this.text,
  }) ;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Row(
      children:[
        Image.asset(
          "assets/image/signup_top.png",
          color: Colors.black,
          width: 20,
        ),
        Text('Kleinc',
          style: AppStyles.titileStyle,
        ),
      ],
    );
  }
}
