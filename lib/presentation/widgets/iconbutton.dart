import 'package:duongguixd/configs/constanst/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class iconbutton_Footer extends StatelessWidget {
  String text;
  String imgSrc;
  // Icon icon;
   Function onTap;
   iconbutton_Footer({
    @required this.imgSrc,
    @required this.onTap,
     @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        width: 80,
        height: 25,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color:Colors.grey ,
          ),
        ),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 3,vertical: 3),
              child:SvgPicture.asset (
              imgSrc,
                width: 10,
                height: 10,
                color: Colors.grey,
              ),
            ),
            Text(text,
              style:  AppStyles.titileStyle.copyWith(fontSize: 10,color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
