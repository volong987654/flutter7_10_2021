import 'package:flutter/material.dart';

class ButtonNext extends StatelessWidget {
  String  text;
  Function onTap;
  final  color, textColor;
  ButtonNext ({
    @required this.text,
    @required this.onTap,
    this.color ,
    this.textColor = Colors.white,
  }) ;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      alignment: Alignment.center,
      width: size.width*0.3,
      height: size.height*0.06,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: GestureDetector(
        onTap: () {
          onTap();
        },
        child: Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold,
              decoration: TextDecoration.none

          ),
        ),
      ),

    );
  }
}
