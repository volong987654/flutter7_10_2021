class AppValues {
  AppValues._();
  static const String appCheckliste = "Save List";

  static const String appName = "RUNNING";
  static const String appHello = "Hello";

  static const String appAmara = "Amara";
  static const String appToday = "Today you run ";
  static const String appFor = "for";
  static const String appKilomerter = "5.31 km";

  static const double inputFormHeight = 55;

  static const String appFireFit = "FireFit";
  static const String appStay = "Stay in shape, stay healthy";
  static const String appSignUp = "Sign Up";
  static const String appSigin = "Login";
  static const String appForgot = "Forgot your password?";
  static const String appUsername = "Username";
  static const String appPassword = "Password";
  static const String appEmail = "Email";
  static const String appConfirmPass = "Confirm password";
  static const String appAccount = "Don`t have an Account? ";
  static const String appForgotPass = "'Forgot your Password?";
  static const String appForgotPassWord = "Forgot Password?";

  static const String appEmailAddress = "Email Address";
  static const String appSend = "Send";
  static const String appRegister = "Register";
  static const String appHaveAcc = "have an Account? ";
  static const String appSignin = "Sign in";
  static const String appStep = "steps";
  static const String appBpm = "bpm";
  static const String appCalories = "calories";
  static const String appDetails = "Details";
}
