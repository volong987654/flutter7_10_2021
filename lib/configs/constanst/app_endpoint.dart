class AppEndpoint {
  AppEndpoint._();

  // ignore: constant_identifier_names
  static const String BASE_URL = "http://relax365.net";

  static const int connectionTimeout = 1500;
  static const int receiveTimeout = 1500;
  static const String keyAuthorization = "Authorization";

  // ignore: constant_identifier_names
  static const int SUCCESS = 200;
  // ignore: constant_identifier_names
  static const int ERROR_TOKEN = 401;
   // ignore: constant_identifier_names
  static const int ERROR_VALIDATE = 422;
   // ignore: constant_identifier_names
  static const int ERROR_SERVER = 500;
   // ignore: constant_identifier_names
  static const int ERROR_DISCONNECT = -1;
 // ignore: constant_identifier_names
  static const String MORE_APPS = '/hsmoreapp';
}
