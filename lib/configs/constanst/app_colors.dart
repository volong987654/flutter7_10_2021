import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static Color title = Color(0xFF033349);
  static Color title300 = Color(0xFF426677);
  static Color green = Color(0xFF64B889);
  static Color grey = Color(0xFFCCCCCC);
  static Color grey300 = Color(0xFFDDDDDD);
  static Color white = Color(0xFFDDDDDD);


  static Color kPrimaryColor = Color(0xFF6F35A5);
  static Color kPrimaryLightColor = Color(0xFFF1E6FF);
  static Color button = Color(0xFF000DAE);
  static Color buttonLogin = Color(0xFFF5F8FA);

}
